from fastapi import APIRouter, Depends, HTTPException, status
from queries.users import (
  UserCategories,
  UserCategoriesCount,
  UserOut,
  UserIn,
  UserQueries
)

from mongo_queries.mongo_users import ( 
  DuplicateUserError,
  MongoUserQueries,
  MongoUserIn,
  MongoUserOut
)


from datetime import date


router = APIRouter()


@router.get("/users/categories", response_model=list[UserCategories])
async def get_user_categories(queries: UserQueries = Depends()):
    try:
        return queries.get_user_categories()
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


# @router.get("/users/{user_id}/", response_model=UserOut)
# async def get_user(
#     user_id: int,
#     repo: UserQueries = Depends(),
# ):
#     return repo.get_user(user_id)

# @router.get("/users/{user_id}", response_model=MongoUserOut)
# async def get_user(user_id: str, queries: MongoUserQueries = Depends()):
#     try:
#         return queries.get_by_id(user_id)
#     except HTTPException as e:
#         # Re-raise the HTTPException caught from the queries class
#         raise e
#     except Exception as e:
#         raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")
    

# @router.post("/users/", response_model=MongoUserOut, status_code=status.HTTP_201_CREATED)
# async def create_user(user_in: MongoUserIn, queries: MongoUserQueries = Depends()):
#     try:
#         return queries.create(user_in)
#     except DuplicateUserError as e:
#         raise HTTPException(status_code=400, detail=str(e))
#     except HTTPException as e:
#         # Re-raise the HTTPException caught from the queries class
#         raise e
#     except Exception as e:
#         raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")



# @router.delete("/users/{user_id}", status_code=status.HTTP_204_NO_CONTENT)
# async def delete_user(user_id: str, queries: MongoUserQueries = Depends()):
#     success = queries.delete(user_id)
#     if not success:
#         raise HTTPException(status_code=404, detail="User not found")
#     return {"ok": True}

# @router.get("/users/", response_model=list[MongoUserOut])
# async def get_all_users(queries: MongoUserQueries = Depends()):
#     try:
#         return queries.get_all_users()
#     except HTTPException as e:
#         # Re-raise the HTTPException caught from the queries class
#         raise e
#     except Exception as e:
#         raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")


# Additional CRUD operations would be added here following the same pattern

@router.post("/users/", response_model=UserOut, status_code=status.HTTP_201_CREATED)
async def create_user(user_in: UserIn, queries: UserQueries = Depends()):
    """
    Create a new user in the database.

    This endpoint will attempt to create a new user with the provided details. 
    If a user with the same username already exists, it will raise a DuplicateUserError.

    Args:
        user_in (UserIn): The user input model containing the details of the new user.
        queries (UserQueries, optional): Dependency that allows interaction with user queries. Defaults to Depends().

    Returns:
        UserOut: The created user's details.

    Raises:
        HTTPException: If a user with the same username already exists (status code 400).
        HTTPException: For any other errors during the creation process (status code 500).
    """
    try:
        return queries.create(user_in)
    except DuplicateUserError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except HTTPException as e:
        # Re-raise the HTTPException caught from the queries class
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")
    




@router.get("/users/{user_id}", response_model=UserOut)
async def get_user(user_id: int, queries: UserQueries = Depends()):
    """
    Retrieve a user based on their unique user ID.

    Args:
        user_id (int): The unique identifier of the user.
        queries (UserQueries, optional): Dependency that allows interaction with user queries. Defaults to Depends().

    Returns:
        UserOut: A user object containing user details.

    Raises:
        HTTPException: An error occurred during the retrieval process.
    """
    try:
        return queries.get_user(user_id)
    except HTTPException as e:
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")




@router.get("/users/", response_model=list[UserOut])
async def get_all_users(queries: UserQueries = Depends()):
    """
    Retrieve all users from the database.

    Args:
        queries (UserQueries, optional): Dependency that allows interaction with user queries. Defaults to Depends().

    Returns:
        list[UserOut]: A list of user objects.

    Raises:
        HTTPException: An error occurred during the retrieval process.
    """
    try:
        return queries.get_all_users()
    except HTTPException as e:
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")
    

@router.put("/users/{user_id}", response_model=UserOut)
async def update_user(user_id: int, user_update: UserIn, queries: UserQueries = Depends()):
    """
    Update an existing user's details in the database.

    This endpoint will attempt to update the details of an existing user identified by the user_id. 
    It will apply the changes provided in the user_update object.

    Args:
        user_id (int): The unique identifier of the user to be updated.
        user_update (UserUpdate): The user input model containing the updated details of the user.
        queries (UserQueries, optional): Dependency that allows interaction with user queries. Defaults to Depends().

    Returns:
        UserOut: The updated user's details.

    Raises:
        HTTPException: If the user with the specified ID does not exist (status code 404).
        HTTPException: For any other errors during the update process (status code 500).
    """
    try:
        updated_user = queries.update(user_id, user_update)
        if updated_user is None:
            raise HTTPException(status_code=404, detail="User not found")
        return updated_user
    except HTTPException as e:
        # Re-raise the HTTPException caught from the queries class
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")
    
@router.delete("/users/{user_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_user(user_id: int, queries: UserQueries = Depends()):
    """
    Delete a user based on their unique user ID.

    Args:
        user_id (int): The unique identifier of the user to be deleted.
        queries (UserQueries, optional): Dependency that allows interaction with user queries. Defaults to Depends().

    Returns:
        dict: A confirmation message indicating successful deletion.

    Raises:
        HTTPException: An error occurred during the deletion process.
    """
    success = queries.delete(user_id)
    if not success:
        raise HTTPException(status_code=404, detail="User not found")
    return {"ok": True}


