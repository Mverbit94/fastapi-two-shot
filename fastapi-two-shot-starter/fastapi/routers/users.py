from fastapi import APIRouter, Depends
from typing import Optional, Union
from queries.users import (
  UserOut,
  UserIn,
  UserQueries
)

from datetime import date


router = APIRouter()





@router.get("/users/{user_id}/", response_model=UserOut)
async def get_user(
    user_id: int,
    repo: UserQueries = Depends(),
):
    return repo.get_user(user_id)