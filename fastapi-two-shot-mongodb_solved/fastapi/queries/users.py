
'''
Users:

Retrieve: SELECT * FROM users WHERE id = :user_id;
Insert: INSERT INTO users (password, username, first_name, last_name, email, date_joined) VALUES (:password, :username, :first_name, :last_name, :email, NOW()) RETURNING id;
'''

from pydantic import BaseModel
from queries.pool import pool
from datetime import date



class UserIn(BaseModel):
    username: str
    first_name: str
    last_name: str
    email: str

class UserOut(BaseModel):
    id: int
    username: str
    first_name: str
    last_name: str
    email: str

class UserQueries:
    def get_user(
          self, user_id: int
    ) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                     SELECT * 
                     FROM users
                     WHERE id = %s;
                    """,
                    [user_id],
                )
                try:
                    record = None
                    for row in cur.fetchall():
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    return record
                except Exception:
                    return {
                        "message": "Could not get user record for this user id"
                    }

