from fastapi import APIRouter, Depends
from typing import Optional, Union
from queries.category import (
  CategoryOut,
  CategoryIn,
  CategoryQueries
)

from datetime import date


router = APIRouter()





@router.get("/categories/{owner_id}", response_model=list[CategoryOut])
async def get_categories(
    owner_id: int,
    repo: CategoryQueries = Depends(),
):
     return repo.get_all_category_for_user(owner_id)


@router.post("/categories/")
async def create_category():
    return "tbd"
