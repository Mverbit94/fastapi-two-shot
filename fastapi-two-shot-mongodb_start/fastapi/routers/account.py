from fastapi import APIRouter, Depends
from typing import Optional, Union
from queries.account import (
    AccountOut,
    AccountIn,
    AccountQueries
)

from datetime import date


router = APIRouter()





@router.get("/accounts/{owner_id}/", response_model=list[AccountOut])
async def get_account(
    owner_id: int,
    repo: AccountQueries = Depends(),
):
    return repo.get_all_accounts_for_user(owner_id)



@router.post("/accounts/")
async def create_account():
    return "tbd"